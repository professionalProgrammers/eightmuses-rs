use crate::Album;
use crate::Error;
use crate::RActive;
use crate::SearchResult;
use scraper::Html;
use serde::de::DeserializeOwned;
use url::Url;

/// An 8muses client
#[derive(Debug, Clone)]
pub struct Client {
    /// The inner http client
    pub client: reqwest::Client,
}

impl Client {
    /// Make a new [`Client`]
    pub fn new() -> Self {
        Self {
            client: reqwest::Client::new(),
        }
    }

    /// Search for a query
    pub async fn search(&self, query: &str) -> Result<SearchResult, Error> {
        let url = Url::parse_with_params("https://comics.8muses.com/search", &[("q", query)])?;
        self.get_json(url.as_str()).await
    }

    /// Get an album
    pub async fn get_album(&self, url: &str) -> Result<Album, Error> {
        self.get_json(url).await
    }

    /// Get the ractive data for a page
    pub async fn get_ractive(&self, url: &str) -> Result<RActive, Error> {
        let text = self
            .client
            .get(url)
            .send()
            .await?
            .error_for_status()?
            .text()
            .await?;

        Ok(tokio::task::spawn_blocking(move || {
            let html = Html::parse_document(&text);
            RActive::from_html(&html)
        })
        .await??)
    }

    /// Internal function to extract the hidden json from an 8muses page
    async fn get_json<T>(&self, url: &str) -> Result<T, Error>
    where
        T: DeserializeOwned,
    {
        let ractive = self.get_ractive(url).await?;
        Ok(serde_json::from_str(&ractive.public)?)
    }
}

impl Default for Client {
    fn default() -> Self {
        Self::new()
    }
}
