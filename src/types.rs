/// An Album
pub mod album;
/// Hidden ractive json data
pub mod ractive;
/// The result of a search
pub mod search_result;

pub use self::album::Album;
pub use self::album::AlbumData;
pub use self::album::Picture;
pub use self::ractive::RActive;
pub use self::search_result::AlbumInfo;
pub use self::search_result::SearchResult;
