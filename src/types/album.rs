use super::AlbumInfo;
use std::collections::HashMap;

/// An Album of pictures and other albums
#[derive(Debug, serde::Deserialize)]
pub struct Album {
    /// Album data
    pub album: AlbumData,

    /// ?
    pub parents: Vec<serde_json::Value>,

    /// ?
    #[serde(rename = "adIndexes")]
    pub ad_indexes: Vec<i32>,

    /// The # of pages
    pub pages: u32,
    /// The current page
    pub page: u32,

    /// ?
    pub u: serde_json::Value,

    /// Pictures in this album
    pub pictures: Vec<Picture>,

    /// ?
    pub user: bool,

    /// ?
    pub sort: String,

    /// ?
    #[serde(rename = "sortOptions")]
    pub sort_options: serde_json::Value,

    /// The sub-albums in this album
    pub albums: Vec<AlbumInfo>,

    /// The tags
    pub tags: Vec<serde_json::Value>,

    /// Unknown data
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

impl Album {
    /// Get the next page url for this [`Album`] if it exists.
    pub fn get_next_page_url(&self) -> Option<String> {
        if self.page >= self.pages {
            return None;
        }

        Some(format!(
            "https://www.8muses.com/comics/album/{}/{}",
            self.album.permalink,
            self.page + 1
        ))
    }
}

/// Album Data
#[derive(Debug, serde::Deserialize)]
pub struct AlbumData {
    /// Whether the album is private?
    #[serde(rename = "isPrivate")]
    pub is_private: bool,

    #[serde(rename = "updatedAt")]
    pub updated_at: String,

    #[serde(rename = "normalizedPath")]
    pub normalized_path: String,

    #[serde(rename = "isCoverLocked")]
    pub is_cover_locked: bool,

    #[serde(rename = "_cover")]
    pub cover: serde_json::Value,

    pub path: String,
    pub id: u32,
    pub name: String,
    pub permalink: String,

    #[serde(rename = "numberViews")]
    pub number_views: u64,

    #[serde(rename = "coverId")]
    pub cover_id: Option<u64>,

    #[serde(rename = "numberLikes")]
    pub number_likes: u64,

    #[serde(rename = "parentId")]
    pub parent_id: Option<u64>,

    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// A url to a picture in this [`Album`].
#[derive(Debug, serde::Deserialize)]
pub struct Picture {
    /// The picture's name
    pub name: String,

    /// The url to a picture
    #[serde(rename = "publicUri")]
    pub public_uri: String,

    /// Unknown data
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

impl Picture {
    /// Get the medium url?
    pub fn get_fm_url(&self) -> String {
        format!("https://www.8muses.com/image/fm/{}.jpg", self.public_uri)
    }

    /// Get the full url?
    pub fn get_fl_url(&self) -> String {
        format!("https://www.8muses.com/image/fl/{}.jpg", self.public_uri)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    const PARSE_ISSUE_1: &str = r##"!LQFQiLN[Q2=3F&gt;QiLQ?2&gt;6QiQxDDF6 `\aQ[Q:5Qia`hg`[QA2E9QiQqt $E@CJ r=F3 r@&gt;:4D^u6=:4:EJ^xDDF6 `\aQ[Q?@C&gt;2=:K65!2E9QiQqt $E@CJ r=F3 r@&gt;:4D^u6=:4:EJ^xDDF6 _____`hhhhhh\_____ahhhhhhQ[Q4@G6Cx5Qi?F==[Q:Dr@G6C{@4&lt;65Qi72=D6[QA6C&gt;2=:?&lt;QiQqt\$E@CJ\r=F3\r@&gt;:4D^u6=:4:EJ^xDDF6\`\aQ[Q:D!C:G2E6Qi72=D6[Q?F&gt;36C':6HDQi`_ggc[Q?F&gt;36C{:&lt;6DQi_[QFA52E65pEQiQa_`g\_g\`g%`fiadiab]___+Q[QA2C6?Ex5Qi`dg`[Q04@G6CQi?F==N[Q2=3F&gt;DQi,.[QA286Qi`[QA286DQi_[QA2C6?EDQi,LQA6C&gt;2=:?&lt;QiQqt\$E@CJ\r=F3\r@&gt;:4DQ[Q?2&gt;6QiQqt $E@CJ r=F3 r@&gt;:4DQN[LQ?2&gt;6QiQu6=:4:EJQ[QA6C&gt;2=:?&lt;QiQqt\$E@CJ\r=F3\r@&gt;:4D^u6=:4:EJQN.[QA:4EFC6DQi,LQ?2&gt;6QiQu6=:4:EJ0!2860_`Q[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fzZw`*"I!`f$s~9;8`e=~d4&gt;rZE:K:)vyyww#gx_qc2~QN[LQ?2&gt;6QiQu6=:4:EJ0!2860_aQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fzpC3xca$\'&gt;rsEv;q:p\){x3~z+&lt;&amp;u{t#A@qJ69+?ZeQN[LQ?2&gt;6QiQu6=:4:EJ0!2860_bQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fwIwh&gt;_`@x'&gt;&gt;=Ct+J@p9(\h4Ju5KKpG$Jbqzhd_a$A)QN[LQ?2&gt;6QiQu6=:4:EJ0!2860_cQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fq\g%rD#HCZa&gt;2!Isd')?w&amp;;:4!Ay$=9v%D5b`*e}CIEQN[LQ?2&gt;6QiQu6=:4:EJ0!2860_dQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fua5&gt;7cw2y=*+\|v8;AhgJJK&gt;sKg?F3'E_w@_xg$)r2~QN[LQ?2&gt;6QiQu6=:4:EJ0!2860_eQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fz&gt;7{4'Icd2d4Hvf~wggp:e*r{urE\@6gvH|(~?!he)fQN[LQ?2&gt;6QiQu6=:4:EJ0!2860_fQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4f!qcvA|qve`I2(&lt;23+2Dg%+t\I@hGGe9"=;u&amp;d&lt;g42*wQN[LQ?2&gt;6QiQu6=:4:EJ0!2860_gQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fu&lt;?'Z\vexws}%ZA}|&gt;~=B'I&gt;9u;I"p\}b(\)I5xhe3}QN[LQ?2&gt;6QiQu6=:4:EJ0!2860_hQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4f{FDf|Z+I%"gG#q&lt;gG_c_E2@yHZsh623{`"r@x%B;~_dQN[LQ?2&gt;6QiQu6=:4:EJ0!2860`_Q[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fzZA%&lt;+$F!}gy8}@{;D"cc;(;f;;;"K@gxI!2{:?BezcQN[LQ?2&gt;6QiQu6=:4:EJ0!2860``Q[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fwGB96Ge3&amp;J)f2~%6)JI%vCJbAuqy5BEb?yr~2&lt;6|:@wQN[LQ?2&gt;6QiQu6=:4:EJ0!2860`aQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fy+*wKZb+%GCqEh;gKt5&amp;H*B&amp;}`w!a'bf62B`+Ke8%GHQN[LQ?2&gt;6QiQu6=:4:EJ0!2860`bQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fpgrA+(_h&gt;$6AF%5yDs_2Cf{|"E+#AsrIpvx*{~wgFJwQN[LQ?2&gt;6QiQu6=:4:EJ0!2860`cQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fw4\a!;`&gt;I?C{u"#chCx'edsq~d\H2`3~_$B&amp;ZfIC!5$QN[LQ?2&gt;6QiQu6=:4:EJ0!2860`dQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fpFcqwJ}_vKHB%ZbD@&gt;5"e%"$&lt;dp8py|HKH'HF!p&lt;4h`QN[LQ?2&gt;6QiQu6=:4:EJ0!2860`eQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fx84*Gg}Dg7zH8Bd(J4v&gt;7856r|864A@h=(*!r'Ev\Z?QN[LQ?2&gt;6QiQu6=:4:EJ0!2860`fQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4f|5#tbAz=*wvzv38%BJKAJ!@gHr3xg22f$*fF;D2Cvr8QN[LQ?2&gt;6QiQu6=:4:EJ0!2860`gQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4f~H=`3p?Z\d{H&amp;fD8hHvJu)3Eq{g%A(zCIC=B`)eFfG_QN[LQ?2&gt;6QiQu6=:4:EJ0!2860`hQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fq%cIA_D4&gt;eqgA`92ypG42$4{5D%cF{b\9&gt;$Ky_gHx)KQN[LQ?2&gt;6QiQu6=:4:EJ0!2860a_Q[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fu(5"ZqZ8q*q+xe5Jt"gFZC$e6s)Gs};&gt;rFJ)cz+w47sQN[LQ?2&gt;6QiQu6=:4:EJ0!2860a`Q[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fpe:&amp;v!z+Zbh(*v=Dph%t=4{@p\bIK*E&amp;gFc"pb~3rv;QN[LQ?2&gt;6QiQu6=:4:EJ0!2860aaQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fy%|bv}!3=8e\'Z!JBC)@zZDdvy3E&lt;qgs5KIAbcG#9fEQN[LQ?2&gt;6QiQu6=:4:EJ0!2860abQ[QAF3=:4&amp;C:QiQBh8xB3=~z?ye`6Crp{9'|@{;:JK{Z!&gt;K5_E@33\\="!suH}}IJ;*{9&gt;&amp;I}7~2hfBeA`ra24v$c6!ycz2&lt;;?4fqz\9xt?p_K%GH:%&lt;=3AJI*zG9b9@9u6'+wr88%J@"g9QN.[QE28DQi,.[QD@CEQiQ2KQ[QFD6CQi72=D6[QD@CE~AE:@?DQiLQG:6HQiQ':6HDQ[Q=:&lt;6QiQ{:&lt;6DQ[Q52E6QiQs2E6Q[Q2KQiQp\+QN[Q25x?56I6DQi,a[h[\`.N"##;

    #[test]
    fn parse_issue_1() {
        let decoded = crate::decode_json_string(PARSE_ISSUE_1).expect("failed to decode");
        let _album: Album = serde_json::from_str(&decoded).expect("failed to parse");
    }
}
