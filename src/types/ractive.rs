use crate::decode_json_string;
use once_cell::sync::Lazy;
use scraper::Html;
use scraper::Selector;

static PUBLIC_TEXT_SELECTOR: Lazy<Selector> =
    Lazy::new(|| Selector::parse("#ractive-public").expect("invalid ractive-public selector"));
static PRIVATE_TEXT_SELECTOR: Lazy<Selector> =
    Lazy::new(|| Selector::parse("#ractive-private").expect("invalid ractive-private selector"));
static SHARED_TEXT_SELECTOR: Lazy<Selector> =
    Lazy::new(|| Selector::parse("#ractive-shared").expect("invalid ractive-shared selector"));

/// An error that may occur while extracting ractive data from html
#[derive(Debug, thiserror::Error)]
pub enum FromHtmlError {
    /// Missing ractive-public
    #[error("missing ractive-public")]
    MissingPublic,

    /// Invalid ractive-public data
    #[error("invalid ractive-public")]
    InvalidPublic,

    /// Missing ractive-private
    #[error("missing ractive-private")]
    MissingPrivate,

    /// Invalid ractive-private data
    #[error("invalid ractive-private")]
    InvalidPrivate,

    /// Missing ractive-shared
    #[error("missing ractive-shared")]
    MissingShared,

    /// Invalid ractive-shared data
    #[error("invalid ractive-shared")]
    InvalidShared,
}

/// Hidden RActive data
#[derive(Debug)]
pub struct RActive {
    /// ractive-public
    pub public: String,

    /// ractive-private
    pub private: String,

    /// ractive-shared
    pub shared: String,
}

impl RActive {
    /// Extract RActive data from html
    pub(crate) fn from_html(html: &Html) -> Result<Self, FromHtmlError> {
        let public = html
            .select(&PUBLIC_TEXT_SELECTOR)
            .next()
            .and_then(|el| el.text().next())
            .ok_or(FromHtmlError::MissingPublic)
            .map(decode_json_string)?
            .ok_or(FromHtmlError::InvalidPublic)?;

        let private = html
            .select(&PRIVATE_TEXT_SELECTOR)
            .next()
            .and_then(|el| el.text().next())
            .ok_or(FromHtmlError::MissingPrivate)
            .map(decode_json_string)?
            .ok_or(FromHtmlError::InvalidPrivate)?;

        let shared = html
            .select(&SHARED_TEXT_SELECTOR)
            .next()
            .and_then(|el| el.text().next())
            .ok_or(FromHtmlError::MissingShared)
            .map(decode_json_string)?
            .ok_or(FromHtmlError::InvalidShared)?;

        Ok(Self {
            public,
            private,
            shared,
        })
    }
}
