use std::collections::HashMap;

/// The result of a search
#[derive(Debug, serde::Deserialize)]
pub struct SearchResult {
    /// ?
    pub u: serde_json::Value,

    /// ?
    #[serde(rename = "sortOptions")]
    pub sort_options: serde_json::Value,

    /// The original query
    pub query: String,
    /// The current page
    pub page: u32,
    /// The total # of pages
    pub pages: u32,

    /// ?
    pub user: bool,

    /// ?
    #[serde(rename = "adIndexes")]
    pub ad_indexes: Vec<i32>,

    /// ?
    pub sort: String,

    /// The albums returned by the search
    pub albums: Vec<AlbumInfo>,

    /// Unknown data
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

/// Info about an [`Album`]
#[derive(Debug, serde::Deserialize)]
pub struct AlbumInfo {
    /// Whether this is private
    #[serde(rename = "isPrivate")]
    pub is_private: bool,

    /// The name of the album.
    ///
    /// This is `None` if this is a private album.
    pub name: Option<Box<str>>,

    /// The cover
    pub cover: Option<Cover>,

    /// The permalink to this album.
    ///
    /// This is `None` if this is a private album.
    pub permalink: Option<Box<str>>,

    /// Unknown data
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}

impl AlbumInfo {
    /// Get the url for this [`AlbumInfo`].
    ///
    /// Returns `None` if this is a private album.
    pub fn get_url(&self) -> Option<String> {
        Some(format!(
            "https://www.8muses.com/comics/album/{}",
            self.permalink.as_ref()?
        ))
    }
}

/// The cover for the [`AlbumInfo`].
#[derive(Debug, serde::Deserialize)]
pub struct Cover {
    /// The uri path fragment for the cover
    #[serde(rename = "publicUri")]
    pub public_uri: String,

    /// Unknown data
    #[serde(flatten)]
    pub unknown: HashMap<String, serde_json::Value>,
}
