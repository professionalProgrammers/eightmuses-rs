#![allow(clippy::or_fun_call)]

/// Client type
pub mod client;
/// Api Types
pub mod types;

pub use self::client::Client;
pub use self::types::Album;
pub use self::types::AlbumData;
pub use self::types::AlbumInfo;
pub use self::types::Picture;
pub use self::types::RActive;
pub use self::types::SearchResult;

/// The error type for this library
#[derive(thiserror::Error, Debug)]
pub enum Error {
    /// A HTTP Error
    #[error("http error")]
    Reqwest(#[from] reqwest::Error),

    /// Failed to parse a url
    #[error("invalid url")]
    Url(#[from] url::ParseError),

    /// Invalid ractive data
    #[error("invalid ractive")]
    InvalidRActive(#[from] crate::types::ractive::FromHtmlError),

    /// Json Error
    #[error("failed to parse json")]
    Json(#[from] serde_json::Error),

    /// A Tokio Join failed
    #[error("failed to join a tokio task")]
    TokioJoin(#[from] tokio::task::JoinError),
}

/// Decode a json string on an 8muses webpage
pub(crate) fn decode_json_string(data: &str) -> Option<String> {
    // TODO: Optimize if allocations need to be avoided
    let data = data
        .trim()
        .replace("&gt;", ">")
        .replace("&lt;", "<")
        .replace("&amp;", "&");

    if data.starts_with('!') {
        data.chars()
            .skip(1)
            .map(|c| match c {
                '!'..='~' => std::char::from_u32(33 + (u32::from(c) + 14) % 94),
                _ => Some(c),
            })
            .collect()
    } else {
        None
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[tokio::test]
    async fn it_works() {
        let client = Client::new();
        let search_results = client.search("test").await.expect("failed to search");
        dbg!(&search_results);
        let album_info = search_results
            .albums
            .first()
            .expect("missing search results");
        let album_url = album_info.get_url().expect("album is private");
        let album = client
            .get_album(&album_url)
            .await
            .expect("failed to get album");
        dbg!(&album);
    }

    #[tokio::test]
    async fn search() {
        let client = Client::new();
        let search_results = client.search("test").await.expect("failed to search");
        dbg!(&search_results);
    }

    #[tokio::test]
    async fn shadbase_works() {
        let client = Client::new();
        let album = client
            .get_album("https://comics.8muses.com/comics/album/ShadBase-Comics")
            .await
            .expect("failed to get album");
        dbg!(&album);
    }
}
