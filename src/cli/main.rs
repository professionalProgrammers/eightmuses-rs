mod commands;

use anyhow::Context;

#[derive(argh::FromArgs)]
#[argh(description = "a CLI to interface with 8muses.com")]
struct Options {
    #[argh(subcommand)]
    subcommand: Subcommand,
}

#[derive(argh::FromArgs)]
#[argh(subcommand)]
enum Subcommand {
    Search(self::commands::search::Options),
    Download(self::commands::download::Options),
}

fn main() -> anyhow::Result<()> {
    let options: Options = argh::from_env();
    let rt = tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .build()
        .context("failed to start tokio runtime")?;
    rt.block_on(async_main(options))?;
    Ok(())
}

async fn async_main(options: Options) -> anyhow::Result<()> {
    let client = eightmuses::Client::new();

    match options.subcommand {
        Subcommand::Search(options) => self::commands::search::exec(client, options).await?,
        Subcommand::Download(options) => self::commands::download::exec(client, options).await?,
    }

    Ok(())
}
