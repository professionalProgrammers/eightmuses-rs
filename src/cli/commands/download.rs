use anyhow::Context;
use std::path::PathBuf;
use std::sync::Arc;
use tokio::fs::File;
use url::Url;

#[derive(argh::FromArgs)]
#[argh(subcommand, name = "download", description = "download an album")]
pub struct Options {
    #[argh(positional, description = "the album url")]
    pub url: Url,

    #[argh(option, default = "PathBuf::new()", description = "the output dir")]
    pub out_dir: PathBuf,
}

pub async fn exec(client: eightmuses::Client, options: Options) -> anyhow::Result<()> {
    let (tx, mut rx) = tokio::sync::mpsc::channel(512);
    let request_semaphore = Arc::new(tokio::sync::Semaphore::new(16));
    spawn_album_download_task(
        client,
        Some(options.url.into()),
        options.out_dir,
        tx,
        request_semaphore,
    );

    while let Some(res) = rx.recv().await {
        match res {
            Ok(msg) => match msg {
                Message::ProcessingAlbum { path } => {
                    eprintln!("Processing Album '{}'...", path);
                }
                Message::DownloadFinished { file_path } => {
                    eprintln!("Downloaded '{}'", file_path.display());
                }
            },
            Err(e) => {
                eprintln!("{:?}", e);
            }
        }
    }

    Ok(())
}

#[derive(Debug)]
pub enum Message {
    ProcessingAlbum { path: String },
    DownloadFinished { file_path: PathBuf },
}

fn spawn_album_download_task(
    client: eightmuses::Client,
    url: Option<String>,
    out_dir: PathBuf,
    tx: tokio::sync::mpsc::Sender<anyhow::Result<Message>>,
    request_semaphore: Arc<tokio::sync::Semaphore>,
) {
    tokio::spawn(async move {
        let album_tx = tx.clone();
        let result = async move {
            let url = url.context("album is private")?;
            let album = get_album_retry(&client, &url, request_semaphore.clone()).await?;

            let _ = tx
                .send(Ok(Message::ProcessingAlbum {
                    path: album.album.path.clone(),
                }))
                .await
                .is_ok();

            // Get upfront to allow moving data out of album
            let next_url = album.get_next_page_url();

            let album_out_dir = out_dir.join(&album.album.permalink);
            tokio::fs::create_dir_all(&album_out_dir).await?;

            if !album.pictures.is_empty() {
                for picture in album.pictures.into_iter() {
                    let file_path = album_out_dir.join(format!("{}.jpg", picture.name));

                    if !file_path.exists() {
                        let client = client.clone();
                        let tx = tx.clone();
                        let request_semaphore = request_semaphore.clone();
                        tokio::spawn(async move {
                            let ret = {
                                let _permit = request_semaphore.acquire().await.is_ok();
                                download_picture(&client, &picture, file_path.clone(), 3)
                                    .await
                                    .with_context(|| {
                                        format!(
                                            "failed to download picture to '{}'",
                                            file_path.display()
                                        )
                                    })
                            };
                            let _ = tx.send(ret).await.is_ok();
                        });
                    }
                }
            }

            for album_info in album.albums.iter() {
                spawn_album_download_task(
                    client.clone(),
                    album_info.get_url(),
                    out_dir.clone(),
                    tx.clone(),
                    request_semaphore.clone(),
                );
            }

            if let Some(next_url) = next_url {
                spawn_album_download_task(
                    client.clone(),
                    Some(next_url),
                    out_dir.clone(),
                    tx,
                    request_semaphore.clone(),
                );
            }

            anyhow::Result::<_>::Ok(())
        }
        .await;

        if let Err(e) = result {
            let _ = album_tx.send(Err(e)).await.is_ok();
        }
    });
}

async fn get_album_retry(
    client: &eightmuses::Client,
    url: &str,
    request_semaphore: Arc<tokio::sync::Semaphore>,
) -> anyhow::Result<eightmuses::Album> {
    let _permit = request_semaphore.acquire().await.is_ok();

    let mut last_error = None;
    let max_attempts = 3;
    for _ in 0..max_attempts {
        let result = client
            .get_album(url)
            .await
            .with_context(|| format!("failed to get album at '{}'", url));

        match result {
            Ok(album) => {
                return Ok(album);
            }
            Err(e) => {
                last_error = Some(e);
            }
        }
    }

    Err(last_error.context("missing error")?)
}

async fn download_picture(
    client: &eightmuses::Client,
    picture: &eightmuses::Picture,
    file_path: PathBuf,
    max_attempts: usize,
) -> anyhow::Result<Message> {
    let mut last_error = None;
    let url = picture.get_fl_url();

    for _ in 0..max_attempts {
        let result = async {
            let tmp_path = pikadick_util::with_push_extension(&file_path, "part");
            let mut file = File::create(&tmp_path)
                .await
                .context("failed to open tmp file")?;
            let mut tmp_path = pikadick_util::DropRemovePath::new(tmp_path);

            pikadick_util::download_to_file(&client.client, url.as_str(), &mut file)
                .await
                .context("failed to download to file")?;

            tokio::fs::rename(&tmp_path, &file_path)
                .await
                .context("failed to rename tmp file")?;
            tmp_path.persist();

            anyhow::Result::<()>::Ok(())
        }
        .await;

        match result {
            Ok(()) => {
                last_error = None;
                break;
            }
            Err(e) => {
                last_error = Some(e);
            }
        }
    }

    if let Some(e) = last_error {
        return Err(e);
    }

    Ok(Message::DownloadFinished { file_path })
}
