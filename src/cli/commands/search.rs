use anyhow::Context;

#[derive(argh::FromArgs)]
#[argh(subcommand, name = "search", description = "search for albums")]
pub struct Options {
    #[argh(positional, description = "the search query")]
    pub query: String,
}

pub async fn exec(client: eightmuses::Client, options: Options) -> anyhow::Result<()> {
    let results = client
        .search(&options.query)
        .await
        .with_context(|| format!("failed to search for '{}'", options.query))?;

    for (i, album) in results.albums.iter().enumerate() {
        let album_name = album.name.as_deref().unwrap_or("<private>");
        let album_link = album.get_url().unwrap_or_else(|| "<private>".to_string());

        println!("#{}", i + 1);
        println!("Name: {album_name}");
        println!("Link: {album_link}");
        println!()
    }

    if results.albums.is_empty() {
        println!("No Results");
    }

    Ok(())
}
