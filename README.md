# eightmuses-rs 

[![Build status](https://ci.appveyor.com/api/projects/status/no91o3ud1705d507?svg=true)](https://ci.appveyor.com/project/professionalProgrammers/eightmuses-rs)
[![](https://tokei.rs/b1/bitbucket/professionalProgrammers/eightmuses-rs)](https://bitbucket.org/professionalProgrammers/eightmuses-rs)

A Rust API for https://8muses.com.

